FLAGS_DEBUG := -Wall -Werror -Wextra -Wunused -Wunreachable-code -pedantic-errors -g

all : affinity

affinity : affinity.c
	gcc -O3 affinity.c -o affinity -lplot

debug : affinity.c
	gcc $(FLAGS_DEBUG) -O0 affinity.c -o affinity -lplot
